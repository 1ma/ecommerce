<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure;

final class HIBPChecker
{
    public function compromised(string $password): bool
    {
        $hash = \strtoupper(\sha1($password));
        $prefix = \substr($hash, 0, 5);

        $stream = \fopen("https://api.pwnedpasswords.com/range/$prefix", 'rb');

        if (!\is_resource($stream)) {
            return $this->fallback($password);
        }

        $suffix = \substr($hash, 5);
        while ($hash = \fgets($stream)) {
            if (0 === \strpos($hash, $suffix)) {
                \fclose($stream);

                return true;
            }
        }

        \fclose($stream);

        return false;
    }

    private function fallback(string $password): bool
    {
        return 8 <= strlen($password);
    }
}
