<?php

declare(strict_types=1);

namespace Ecommerce\Infrastructure;

/**
 * Argon2ID hasher
 */
final class ArgonHasher
{
    /**
     * @var int
     */
    private $memoryCost;

    /**
     * @var int
     */
    private $timeCost;

    /**
     * @var int
     */
    private $threads;

    public function __construct(int $memoryCost, int $timeCost, int $threads)
    {
        $this->memoryCost = $memoryCost;
        $this->timeCost = $timeCost;
        $this->threads = $threads;
    }

    public static function default(): ArgonHasher
    {
        return new ArgonHasher(
            PASSWORD_ARGON2_DEFAULT_MEMORY_COST,
            PASSWORD_ARGON2_DEFAULT_TIME_COST,
            PASSWORD_ARGON2_DEFAULT_THREADS
        );
    }

    public function hash(string $plainText): string
    {
        return \password_hash($plainText, PASSWORD_ARGON2ID, [
            'memory_cost' => $this->memoryCost,
            'time_cost' => $this->timeCost,
            'threads' => $this->threads
        ]);
    }
}
