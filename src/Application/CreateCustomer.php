<?php

declare(strict_types=1);

namespace Ecommerce\Application;

use DateTime;
use DateTimeImmutable;
use Ecommerce\Infrastructure\ArgonHasher;
use Ecommerce\Infrastructure\HIBPChecker;
use PDO;
use PDOStatement;
use stdClass;
use UMA\JsonRpc;
use UMA\Uuid\UuidGenerator;

/**
 * Failure scenarios:
 *  - Duplicate email
 *  - Password compromised (checked via HIBP)
 */
final class CreateCustomer implements JsonRpc\Procedure
{
    /**
     * @var stdClass
     */
    private static $contract;
    /**
     * @var PDO
     */
    private $rw;

    /**
     * @var HIBPChecker
     */
    private $hibp;

    /**
     * @var UuidGenerator
     */
    private $uuid;

    /**
     * @var ArgonHasher
     */
    private $hasher;

    /**
     * @var PDOStatement
     */
    private $customerStmt;

    public function __construct(PDO $rw, HIBPChecker $hibp, UuidGenerator $uuid, ArgonHasher $hasher)
    {
        $this->rw = $rw;
        $this->hibp = $hibp;
        $this->uuid = $uuid;
        $this->hasher = $hasher;
        $this->customerStmt = $this->rw->prepare('
INSERT INTO customers (id, full_name, email_address, password_hash, billing_address, country, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)
        ');
    }

    public function __invoke(JsonRpc\Request $request): JsonRpc\Response
    {
        $data = $request->params();

        if ($this->hibp->compromised($data->password)) {
            return new JsonRpc\Error(123, 'sorry, you are fucked', null, $request->id());
        }

        $id = $this->uuid->generate();
        $hash = $this->hasher->hash($data->password);

        $this->rw->beginTransaction();

        $this->customerStmt->bindValue(1, $id->asBytes(), PDO::PARAM_LOB);
        $this->customerStmt->bindValue(2, $data->fullName);
        $this->customerStmt->bindValue(3, strtolower($data->emailAddress));
        $this->customerStmt->bindValue(4, $hash, PDO::PARAM_LOB);
        $this->customerStmt->bindValue(5, $data->billingAddress);
        $this->customerStmt->bindValue(6, $data->country);
        $this->customerStmt->bindValue(7, (new DateTimeImmutable('now'))->format(DateTime::ATOM));

        if(false === $this->customerStmt->execute()) {
            $this->rw->rollBack();

            return new JsonRpc\Error(456, 'rekt', null, $request->id());
        }

        $this->customerStmt->closeCursor();

        $this->rw->commit();

        return new JsonRpc\Success($request->id(), ['id' => $id->asString()]);
    }

    public function getSpec(): ?stdClass
    {
        if (null === self::$contract) {
            self::$contract = \json_decode(\file_get_contents(__DIR__  . '/CreateCustomer.json'));
        }


        return self::$contract;
    }
}
