<?php

declare(strict_types=1);

namespace Ecommerce\Application;

use DateTime;
use DateTimeImmutable;
use PDO;
use PDOStatement;
use stdClass;
use UMA\JsonRpc;
use UMA\Uuid\UuidGenerator;

/**
 * Failure scenarios:
 *  - The product needs shipping but the initial stock is not supplied
 */
final class CreateProduct implements JsonRpc\Procedure
{
    /**
     * @var stdClass
     */
    private static $contract;

    /**
     * @var PDO
     */
    private $rw;
    /**
     * @var UuidGenerator
     */
    private $uuid;

    /**
     * @var PDOStatement
     */
    private $productStmt;

    /**
     * @var PDOStatement
     */
    private $pricesStmt;

    public function __construct(PDO $rw, UuidGenerator $uuid)
    {
        $this->rw = $rw;
        $this->uuid = $uuid;

        $this->productStmt = $this->rw->prepare('
INSERT INTO products (sku, description, needs_shipping, stock, created_at) VALUES (?, ?, ?, ?, ?)
        ');

        $this->pricesStmt = $this->rw->prepare('
INSERT INTO product_prices (sku, currency, base_price, set_at) VALUES (?, ?, ?, ?)
        ');
    }

    public function __invoke(JsonRpc\Request $request): JsonRpc\Response
    {
        $data = $request->params();

        if (true === $data->needsShipping && !\property_exists($data, 'initialStock')) {
            return new JsonRpc\Error(123, 'fuck', null, $request->id());
        }

        $this->rw->beginTransaction();

        $sku = $this->uuid->generate();
        $this->productStmt->bindValue(1, $sku->asBytes(), PDO::PARAM_LOB);
        $this->productStmt->bindValue(2, $data->description);
        $this->productStmt->bindValue(3, $data->needsShipping, PDO::PARAM_BOOL);
        if (\property_exists($data, 'initialStock')) {
            $this->productStmt->bindValue(4, $data->initialStock, PDO::PARAM_INT);
        } else {
            $this->productStmt->bindValue(4, null, PDO::PARAM_NULL);
        }
        $this->productStmt->bindValue(5, $now = (new DateTimeImmutable('now'))->format(DateTime::ATOM));

        if(false === $this->productStmt->execute()) {
            $this->productStmt->closeCursor();
            $this->rw->rollBack();

            return new JsonRpc\Error(456, 'rekt', null, $request->id());
        }

        $this->productStmt->closeCursor();

        foreach($data->prices as $currency => $price) {
            $this->pricesStmt->bindValue(1, $sku->asBytes(), PDO::PARAM_LOB);
            $this->pricesStmt->bindValue(2, $currency);
            $this->pricesStmt->bindValue(3, $price);
            $this->pricesStmt->bindValue(4, $now);

            if(false === $this->pricesStmt->execute()) {
                $this->pricesStmt->closeCursor();
                $this->rw->rollBack();

                return new JsonRpc\Error(456, 'rekt', null, $request->id());
            }

            $this->pricesStmt->closeCursor();
        }

        $this->rw->commit();

        return new JsonRpc\Success($request->id(), ['sku' => $sku->asString()]);
    }

    public function getSpec(): ?stdClass
    {
        if (null === self::$contract) {
            self::$contract = \json_decode(\file_get_contents(__DIR__  . '/CreateProduct.json'));
        }

        return self::$contract;
    }
}
