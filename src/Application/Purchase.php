<?php

declare(strict_types=1);

namespace Ecommerce\Application;

use stdClass;
use UMA\JsonRpc;

/**
 * Failure scenarios:
 *  - Unknown customer ID
 *  - Unknown payment currency
 *  - One or more product SKUs are unknown
 *  - At least one product needs shipping but a shipping address is not supplied
 *  - One or more products are not available in the requested currency
 *  - One or more products do not have sufficient stock to fulfill the order
 */
final class Purchase implements JsonRpc\Procedure
{
    /**
     * @var stdClass
     */
    private static $contract;

    public function __invoke(JsonRpc\Request $request): JsonRpc\Response
    {
        return new JsonRpc\Success($request->id());
    }

    public function getSpec(): ?stdClass
    {
        if (null === self::$contract) {
            self::$contract = \json_decode(\file_get_contents(__DIR__  . '/Purchase.json'));
        }

        return self::$contract;
    }
}
