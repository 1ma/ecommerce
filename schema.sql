CREATE TABLE countries
(
    code     BLOB PRIMARY KEY CHECK (LENGTH(code) = 2),
    currency BLOB NOT NULL CHECK (LENGTH(currency) = 3),
    vat_rate REAL NOT NULL CHECK (0 <= vat_rate AND vat_rate <= 1.0)
);

INSERT INTO countries (code, currency, vat_rate)
VALUES ('AD', 'EUR', 0.00),
       ('ES', 'EUR', 0.21),
       ('PT', 'EUR', 0.23),
       ('UK', 'GBP', 0.20),
       ('US', 'USD', 0.10);

CREATE TABLE products
(
    sku            BLOB PRIMARY KEY CHECK (LENGTH(sku) = 16),
    description    TEXT    NOT NULL,
    needs_shipping INTEGER NOT NULL CHECK (needs_shipping = 0 OR needs_shipping = 1),
    stock          INTEGER NULL CHECK (stock IS NULL OR 0 <= stock),
    created_at     TEXT    NOT NULL
);

CREATE TABLE product_prices
(
    sku        BLOB    NOT NULL CHECK (LENGTH(sku) = 16),
    currency   BLOB    NOT NULL,
    base_price INTEGER NOT NULL CHECK (0 <= base_price),
    set_at     TEXT    NOT NULL,
    PRIMARY KEY (sku, currency),
    FOREIGN KEY (sku) REFERENCES products (sku),
    FOREIGN KEY (currency) REFERENCES countries (currency)
);

CREATE TABLE customers
(
    id              BLOB PRIMARY KEY CHECK (LENGTH(id) = 16),
    full_name       TEXT NOT NULL,
    email_address   TEXT NOT NULL UNIQUE,
    password_hash   BLOB NOT NULL,
    billing_address TEXT NOT NULL,
    country         BLOB NOT NULL,
    created_at      TEXT NOT NULL,
    FOREIGN KEY (country) REFERENCES countries (code)
);

CREATE TABLE orders
(
    id                 BLOB PRIMARY KEY CHECK (LENGTH(id) = 16),
    customer_id        BLOB    NOT NULL,
    vat_rate           REAL    NOT NULL CHECK (0 <= vat_rate AND vat_rate <= 1.0),
    subtotal_after_tax INTEGER NOT NULL,
    payment_currency   BLOB    NOT NULL CHECK (LENGTH(payment_currency) = 3),
    payment_status     TEXT    NOT NULL,
    created_at         TEXT    NOT NULL
);

CREATE TABLE order_items
(
    order_id            BLOB    NOT NULL,
    product_sku         BLOB    NOT NULL,
    number_of_items     INTEGER NOT NULL CHECK (1 <= number_of_items),
    subtotal_before_tax INTEGER NOT NULL CHECK (1 <= subtotal_before_tax),
    FOREIGN KEY (order_id) REFERENCES orders (id)
);

CREATE TABLE shipments
(
    order_id         BLOB NOT NULL,
    delivery_status  TEXT NOT NULL,
    delivery_address TEXT NOT NULL,
    delivery_date    TEXT NOT NULL,
    delivery_fee     INTEGER CHECK (0 <= delivery_fee),
    FOREIGN KEY (order_id) REFERENCES orders (id)
);
