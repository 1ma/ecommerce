# e-commerce Domain

Here in this project we will be modeling the foundations of an international e-commerce system following a DDD approach.
This document outlines its (very basic) domain.

## Ubiquitous Language Dictionary

### Customer

A Customer is a stakeholder of the e-commerce that uses it to purchase Products.

Customers are internally identified with an UUID, but they also have a *full name*, a *billing address*, and a unique *email address*.

Customers are also tied to a Country, which determines which currency they will use to make purchases and which prices they
will be charged when they buy Products.


### Country

The system maintains a list of Countries where it is able to sell Products. Each Country is identified with its *ISO 3166 code*, and
we also store its current *VAT rate* and official *currency*.

The same currency can obviously be used in several countries, such as in the case of the Euro.


### Product

A Product is any given item we will sell through the e-commerce system. Products are identified by an *SKU*, that in 
this system will be modeled as an UUID for convenience. They also have a non-optional, human-readable *description*.

Products may *need shipping* or not, because they aren't necessarily physical (e.g. a digital download or a software serial key).

Products may have a *stock* or not, meaning the remaining number of Products of this type that are still left in the warehouse for selling.

Regarding their prices, a Product may be sold in several different currencies. These prices are not related together in any way
(a Product could possibly be 50% cheaper in GBP than in USD at current exchange rates). Individual Product prices are always
before taxes, because these vary depending on the Country of the Customer.


### Physical Product

A synonym for a Product that needs shipping.

A particularity of physical products is that they always have stock, since they are tangible objects.


### Digital Product

A synonym for a Product that can be downloaded and so it does not need shipping. Digital products may not have stock
(for instance a software download) but some do (a serial key is intangible, but still we might have a finite amount of them).


### Order

An Order is a purchase of one or several Products made by a Customer at a given point in time. Each Product in the Order can
be bought multiple times (e.g. an Order of 1 cell phone and 2 spare batteries). Each Order is identified with its own UUID,
which could also act as a kind of invoice reference.

When an Order contains at least one Product that needs shipping it will have an associated Shipment. If it doesn't it will not.

Orders hold the full price of the purchase (a.k.a subtotal), which is the sum of the prices of all bought products times
plus the delivery fee if applicable, then all multiplied by the Country's VAT rate.

Subtotal: (p1 + p2 + ... pN + optional delivery_fee)*(1 + VAT/100)

Orders have a payment status (pending, paid, refunded).

Successful Orders (those whose payment status is "paid") must decrease the stock of their physical products, if applicable.

It is also important to keep in mind that Customers, Products and their prices may come and go from the system, but an Order
is more like a record. All (or almost all?) of its information should be stored redundantly to ensure it is preserved in
the long term. Even VAT rates and official currencies can change over the years. None of these changes should disrupt or
change historical Orders in any way.


## Shipment

A Shipment is always tied to a "physical" Order, and records the delivery date, delivery status (pending, successful, refunded)
and the delivery fee of the Order.

For simplification purposes, an Order containing physical Products will never be split in several Shipments, they'll all
ship together.


### ISO 3166

An ISO standard to identify countries with 2-letter codes. For instance AD is Andorra.

https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes


### ISO 4217

An ISO standard to identify currencies with 3-letter codes. For instance USD is the United States Dollar.

https://en.wikipedia.org/wiki/ISO_4217#Active_codes


### SKU

Stock Keeping Unit, the unique identifier of a Product.
In our e-commerce system we model them as UUID, which is convenient for this exercise, but not in the real world.


### UUID

Universally Unique Identifier.

Example: 59753e66-5f52-4b38-bd9d-3e81fb7abb2c


## Use Cases

In summary, this first iteration of the system will allow just the following three transactional operations. Each one of
these operations will be implemented as a JSON-RPC 2.0 method and backed with a json-schema contract.

* Create a new Product to sell.
* Create a new Customer so that he can buy Products on the platform.
* As a Customer, purchase one or several Products

## CreateProduct

To create a new product we need a mandatory description, a "needsShipping" boolean flag, and a map of prices in
several currencies (though the map can also be empty: the product will have no price and would not be possible to sell it).
For now only three currencies are supported (EUR, GBP and USD), which are hardcoded straight into the contract.

When "needsShipping" is true, an "initialStock" property is also expected, representing the initial amount of available
stock for the new product.

A more realistic system would probably allow the product creator to define the SKU he wants, but in this case we'll
let the system autogenerate a UUID and return it.

Passing prices as json floating point numbers is also not ideal, but since this is a toy project and working in base 100
integers is tedious we'll let that slide.

Identified failure scenarios:

- The product needs shipping, but the optional "initialStock" property was not supplied.

Sample payloads:

```
{
  "description": "GTA V Steam Key",
  "needsShipping": false,
  "prices": {
    "EUR": 29.99,
    "GBP": 24.95
  }
}

{
  "description": "Garden chair",
  "needsShipping": true,
  "initialStock": 25
  "prices": {
    "GBP": 12.99
  }
}

{
  "description": "Unobtanium",
  "needsShipping": false,
  "prices": {}
}
```
